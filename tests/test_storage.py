import io
from filabase import storage as st
import pathlib
import unittest.mock
import os


class TestStorage:
    """Unit tests for Storage"""

    def test_init_sets_db_path(self):
        """after initiating, self.db_path should be a pathlib.Path object
        with expanded home directory"""
        sample_path = pathlib.Path('~/test/dir').expanduser()
        storage = st.Storage('~/test/dir')
        assert storage.db_path == sample_path

    def test_persist_inits_data_dir_if_needed(self):
        """
        When invoking the persist method before creating the given database
        directory:
        The data directory is created.

        """
        # setup
        storage = st.Storage("~/test/dir")
        storage.init_datadir = unittest.mock.Mock(return_value=None)
        storage.write_data = unittest.mock.Mock(return_value=0)

        # act
        storage.persist("shrubbery", [{"shrubbery_id": "shrubs"}])

        # examine
        storage.init_datadir.assert_called_once()

    def test_persists_serialized_records_in_record_db(self):
        """
        When invoking persist, the records are written
        into the correct db file
        """
        # setup
        storage = st.Storage("~/test/dir")
        storage.db_exists = unittest.mock.Mock(return_value=True)

        expected_write_string = "{\"shrubbery_id\": \"shrubs\"}" + os.linesep

        mock_file = io.StringIO()
        mock_file.write = unittest.mock.MagicMock()
        mock_file.write.return_value = len(expected_write_string)

        m_open = unittest.mock.mock_open()
        m_open.return_value = mock_file

        # act
        with unittest.mock.patch('builtins.open', m_open):
            byte_count = storage.persist("shrubbery",
                                         [{"shrubbery_id": "shrubs"}])
            # examine
            assert byte_count == len(expected_write_string)

        # examine
        mock_file.write.assert_called_once_with(expected_write_string)

    def test_extract_return_empty_when_non_existing_record_type(self):
        """
            When the user requests a record_type which does not exist,
            an empty list is returned
        """
        # setup
        db_path = unittest.mock.Mock(spec=pathlib.Path())
        db_path.joinpath.return_value = unittest.mock.Mock(spec=pathlib.Path())
        db_path.joinpath.return_value.exists.return_value = False

        storage = st.Storage("~/test/dir")
        storage.db_path = db_path

        # act and examine
        assert storage.extract("shrubbery") == []

    def test_extract_returns_file_contents_as_list_of_dicts(self):
        """
            if there are previously persisted records, read and present as list
            of dictionaries
        """
        db_path = unittest.mock.Mock(spec=pathlib.Path())
        storage = st.Storage("~/test/dir")
        storage.db_path = db_path
        db_path.joinpath.return_value = unittest.mock.Mock(spec=pathlib.Path())
        db_path.joinpath.return_value.exists.return_value = True

        file_contents = "{\"shrubbery_id\": \"shrubs\"}\n{\"knights\": \"Ni\"}"

        mock_file = io.StringIO(initial_value=file_contents)

        m_open = unittest.mock.mock_open()
        m_open.return_value = mock_file

        # act
        with unittest.mock.patch('builtins.open', m_open):
            results = storage.extract("shrubbery")
            # examine
            assert isinstance(results, list)
            assert len(results) == 2
            assert results[0] == {"shrubbery_id": "shrubs"}
            assert results[1] == {"knights": "Ni"}

    def test_delete_data_removes_record_from_database_file(self):
        """
            remove designated record from file storage, keeping everything else
        """
        # setup
        storage = st.Storage("~/test/dir")
        storage.db_exists = unittest.mock.Mock(return_value=True)
        storage.db_file_exists = unittest.mock.MagicMock(return_value=True)
        storage.compose_datapath = unittest.mock.MagicMock(
                return_value="shrubbery")

        expected_write_string = "{\"shrubbery_id\": \"shrubs\"}" + os.linesep
        # to_be_deleted = "{\"shrubbery_id\": \"nuts\"}" + os.linesep
        initial_records = ["{\"shrubbery_id\": \"nuts\"}",
                           "{\"shrubbery_id\": \"shrubs\"}"]

        storage.write_data = unittest.mock.MagicMock()
        storage.read_datafile = unittest.mock.MagicMock(
                return_value=initial_records)

        # act
        storage.delete_data("shrubbery", filter_by={"shrubbery_id": "nuts"})
        # examine
        storage.write_data.assert_called_once_with("shrubbery",
                                                   expected_write_string)
