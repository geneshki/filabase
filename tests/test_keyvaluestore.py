import pathlib
from filabase import keyvaluestore
from filabase import storage as st
import unittest


class TestKeyValueStore:
    """Unit tests for KeyValueStore"""

    def test_init_creates_store_dict(self):
        """
            Creating a KeyValueStore creates an object with a store property
        """
        store = keyvaluestore.KeyValueStore()
        assert store.store is not None
        assert isinstance(store.store, dict)

    def test_init_creates_storage_io(self):
        """
            instantiating the KeyValueStore creates an io module
        """
        expected_path = pathlib.Path("~/.filabase").expanduser()
        store = keyvaluestore.KeyValueStore()
        assert isinstance(store.io, st.Storage)
        assert store.io.db_path == expected_path

    def test_put_inserts_key_value_pair_into_store(self):
        """
            inserts the given key and value into the store dictionary
        """
        store = keyvaluestore.KeyValueStore()
        store.put('a', 'b')

        assert len(store.store) == 1
        assert store.store['a'] == 'b'

    def test_persist_invokes_storage(self):
        """
            when persist is called the storage is passed the entire set of
            key-value pairs to be written into a file.
        """
        store = keyvaluestore.KeyValueStore()
        store.put('a', 'b')
        store.io.persist = unittest.mock.MagicMock()

        store.persist()

        store.io.persist.assert_called_with("keyvaluepairs", [{'a': 'b'}])

    def test_give_returns_None_if_key_doesnt_exist(self):
        """
            when a key which hasn't been stored is asked,
            a None value is returned
        """
        store = keyvaluestore.KeyValueStore()
        store.store['a'] = 'b'

        assert store.give('b') is None

    def test_give_returns_value_stored_under_given_key(self):
        """
            when the give method is invoked with certain key,
            the value for this key is returned
        """
        store = keyvaluestore.KeyValueStore()
        store.store['a'] = 'b'

        assert store.give('a') == 'b'

    def test_seive_returns_empty_dict_when_key_does_not_match_key_regex(self):
        """
            when invoking seive with given regex for key, the result will be
            an empty dict if no key matches the given regex
        """
        store = keyvaluestore.KeyValueStore()
        store.store['a'] = 'b'

        assert store.seive('c') == {}

    def test_seive_returns_dict_with_key_value_pairs_matching_the_regex(self):
        """test_seive_returns_dict_with_key_value_pairs_matching_the_regex"""

        store = keyvaluestore.KeyValueStore()
        store.store['a'] = 'b'

        assert store.seive("[a-z]") == {'a': 'b'}

    def test_load_reads_persisted_store(self):
        """
            When the load method is called, the facility reads the contents of
            ~/.filabase/keyvaluepairs
        """
        store = keyvaluestore.KeyValueStore()
        store.io.extract = unittest.mock.MagicMock()

        store.load()

        store.io.extract.assert_called_with('keyvaluepairs')

    def test_load_inserts_extracted_pairs_into_store(self):
        """
            When the load method is called, the pairs persisted on media are
            inserted into the store.
        """

        store = keyvaluestore.KeyValueStore()
        store.io.extract = unittest.mock.MagicMock()
        store.io.extract.return_value = [{'a': 'b'}]

        store.load()

        assert 'a' in store.store
        assert store.store['a'] == 'b'

    def test_load_overrides_existing_pairs_in_the_store(self):
        """
            When the load method is called, the contents of the persistance
            media is read. Then the key-value pairs are inserted into the hot
            store.
            If there are key-value pairs in the hot store whose key matches any
            of the keys in the persistance media, their value will be
            overriden.

            I.e. the persistance media always has precedence.
        """

        store = keyvaluestore.KeyValueStore()
        store.io.extract = unittest.mock.MagicMock()
        store.io.extract.return_value = [{'a': 'b', 'c': 'good'}]

        store.store['c'] = 'bad'

        store.load()

        assert 'c' in store.store
        assert store.store['c'] == 'good'

    def test_load_keeps_stored_items_if_persistance_media_is_empty(self):
        """
            When the persistence media is empty or non-existing,
            the items already in store remain unchanged.
        """

        store = keyvaluestore.KeyValueStore()
        store.io.extract = unittest.mock.MagicMock()
        store.io.extract.return_value = []

        store.store['c'] = 'good'

        store.load()

        assert 'c' in store.store
        assert store.store['c'] == 'good'
